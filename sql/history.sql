-- Création schema/tables pour la demo
CREATE SCHEMA fp_vegetation;

CREATE TABLE fp_vegetation.t_arbre_type
(
id serial,
nom text,
nom_sci text,
PRIMARY KEY(id)
)

CREATE TABLE fp_vegetation.arbre (
id serial,
geom geometry(Point, 3945),
id_type int,
date_creat date,
date_maj date,

PRIMARY KEY(id),
   CONSTRAINT fk_id_type
      FOREIGN KEY(id_type) 
	  REFERENCES fp_vegetation.t_arbre_type(id)
)

INSERT INTO fp_vegetation.t_arbre_type (nom, nom_sci) VALUES ('Hêtre commun', 'Fagus sylvatica');
INSERT INTO fp_vegetation.t_arbre_type (nom, nom_sci) VALUES ('Tilleul à petites feuilles', 'Tilia cordata');
INSERT INTO fp_vegetation.t_arbre_type (nom, nom_sci) VALUES ('Laurier', 'Laurus Nobilis');

-- Insertion de l'outil d'audit
-- https://github.com/qwat/pg-history-viewer
SELECT audit.audit_table('fp_vegetation.arbre');
