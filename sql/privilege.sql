-- Privilège

-- Création schema/tables pour la demo
CREATE SCHEMA fp_vegetation;

CREATE TABLE fp_vegetation.t_arbre_type
(
id serial,
nom text,
nom_sci text,
PRIMARY KEY(id)
)

CREATE TABLE fp_vegetation.arbre (
id serial,
geom geometry(Point, 3945),
id_type int,
date_creat date,
date_maj date,

PRIMARY KEY(id),
   CONSTRAINT fk_id_type
      FOREIGN KEY(id_type) 
	  REFERENCES fp_vegetation.t_arbre_type(id)
)

-- Droit additif, donc il faut d'abord supprimer les droits sur la table
REVOKE SELECT ON "table" FROM specific_user;

-- Puis autoriser toutes les colonnes, sauf celles non intéressantes
GRANT SELECT (<all columns except "column1" and "column2">)
   ON "table" TO specific_user;

GRANT USAGE ON SCHEMA fp_vegetation TO loic;
GRANT SELECT ON fp_vegetation.t_arbre_type TO loic;


REVOKE SELECT ON fp_vegetation.t_arbre_type FROM loic;


GRANT SELECT (nom) ON fp_vegetation.t_arbre_type TO loic;

SELECT * et nom FROM fp_vegetation.t_arbre_type

GRANT UPDATE (nom) ON TABLE fp_vegetation.t_arbre_type TO loic

UPDATE fp_vegetation.t_arbre_type  SET nom = 'laurier' where nom = 'Laurier' -- avec un utilisateur super_user
SELECT nom FROM fp_vegetation.t_arbre_type

UPDATE fp_vegetation.t_arbre_type  SET nom = 'Laurier' where nom = 'laurier' -- avec loic
SELECT nom FROM fp_vegetation.t_arbre_type
